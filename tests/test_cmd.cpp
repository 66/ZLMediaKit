/******************************************************************************
 *
 *       Filename:  test_cmd.cpp
 *
 *    Description:  : test cmd
 *
 *        Version:  1.0
 *        Created:  2020年11月12日 22时37分11秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  yangkun (yk)
 *          Email:  xyyangkun@163.com
 *        Company:  yangkun.com
 *
 *****************************************************************************/

#include <signal.h>
#include <iostream>
#include "Common/config.h"
#include "Shell/ShellSession.h"
#include "Network/TcpServer.h"


using namespace toolkit;
using namespace mediakit;

namespace Shell {

}

int main()
{
	cout << "yk debug" << endl;
	string ini = "test.ini";	
	
	mINI mini;
	mini["one.item1"] = "1";
	mini["one.item2"] = "2";
	for(auto &v: mini)
		cout << " " << v.first << " " << v.second << endl;
	cout << mini.dump();

	mini.dumpFile();
	
	int shellPort = 12000;
    TcpServer::Ptr shellSrv(new TcpServer());
    shellSrv->start<ShellSession>(shellPort);

    //设置退出信号处理函数
    static semaphore sem;
    signal(SIGINT, [](int) { sem.post(); });// 设置退出信号
    signal(SIGHUP, [](int) { loadIniConfig(); });
    sem.wait();


}
